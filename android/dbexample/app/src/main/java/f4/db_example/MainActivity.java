package f4.db_example;

import android.os.AsyncTask;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.KeyEvent;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;

import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class MainActivity extends AppCompatActivity {
    private List<Data> recData = new ArrayList<>();
    private DataAdapter mAdapter;

    String user_send;
    String msg_send;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        RecyclerView recyclerView = findViewById(R.id.recycler_view);

        final EditText username = findViewById(R.id.username);
        final EditText message = findViewById(R.id.message);
        message.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    user_send = username.getText().toString();
                    msg_send = message.getText().toString();
                    submitData setData = new submitData();
                    setData.execute();
                    handled = true;
                }
                return handled;
            }
        });

        mAdapter = new DataAdapter(recData);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getApplicationContext());
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(mAdapter);

        getData prepareData = new getData();
        prepareData.execute();
        }

    private class getData extends AsyncTask<Void, Void, Void> {
        final String URL = "http://192.168.43.99:80/web";

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String user;
                String msg;
                Document site = Jsoup.connect(URL).get();
                Elements table = site.select("tr#data");
                for (Element data : table) {
                    Elements td = data.select("td");

                    user = td.get(0).text();
                    msg = td.get(1).text();

                    Data sendData = new Data(user, msg);
                    recData.add(sendData);
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mAdapter.notifyDataSetChanged();
        }
    }

    private class submitData extends AsyncTask<Void, Void, Void> {
        final String URL = "http://192.168.43.99:80/web/dbconnect.php";

        HashMap<String, String> formData = new HashMap<>();

        submitData() {
            formData.put("user", user_send);
            formData.put("msg", msg_send);
        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                Connection.Response send = Jsoup.connect(URL)
                        .data(formData)
                        .method(Connection.Method.POST)
                        .execute();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            recData.clear();
            getData prepareData = new getData();
            prepareData.execute();
        }
    }
}

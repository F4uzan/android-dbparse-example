package f4.db_example;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

public class DataAdapter extends RecyclerView.Adapter<DataAdapter.ViewHolder> {
        private List<Data> recData;

        public class ViewHolder extends RecyclerView.ViewHolder {
            public TextView user, msg;

            public ViewHolder(View view) {
                super(view);
                user = (TextView) view.findViewById(R.id.user);
                msg = (TextView) view.findViewById(R.id.msg);
            }
        }

    public DataAdapter(List<Data> recData) {
        this.recData = recData;
    }

        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View itemView = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.db_content, parent, false);

            return new ViewHolder(itemView);
        }

        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            Data data = recData.get(position);
            holder.user.setText(data.getUser());
            holder.msg.setText(data.getMsg());
        }

        @Override
        public int getItemCount() {
            return recData.size();
        }
}

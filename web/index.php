<html>
<table style='border: solid 1px black;'>
<?php

$servername = "localhost";
$username = "user1";
$password = "pass";
$db = "web";

try {
    $conn = new PDO("mysql:host=$servername;dbname=$db", $username, $password);
    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $stmt = $conn->prepare("SELECT user, msg FROM content");
    $stmt->execute();

    $result = $stmt->setFetchMode(PDO::FETCH_ASSOC); 
    while ($row = $stmt->fetch()) {
    echo "<tr id='data' name='data'>";
    echo "<td style='width:120px;border:1px solid black;'>" . $row['user'] . "</td>";
    echo "<td style='width:120px;border:1px solid black;'>" . $row['msg'] . "</td>";
    echo "</tr>";
    echo "\n";
   }
}
catch(PDOException $e) {
    echo "Connection failed: " . $e->getMessage();
}

$conn = null;
?>
</table>

<br>
<form action="dbconnect.php" id="msgtab" method="post">
	User: <input type="text" name="user" id="user"><br>
	Message:<br><textarea rows="4" name="msg" id="msg"></textarea>
	<br><input type="submit">
</form>
</html>